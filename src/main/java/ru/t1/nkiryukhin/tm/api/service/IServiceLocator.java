package ru.t1.nkiryukhin.tm.api.service;

import ru.t1.nkiryukhin.tm.api.repository.IAuthService;
import ru.t1.nkiryukhin.tm.api.repository.IUserService;

public interface IServiceLocator {

    IAuthService getAuthService();

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IUserService getUserService();

}
