package ru.t1.nkiryukhin.tm.api.repository;

import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.EmailEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.LoginEmptyException;
import ru.t1.nkiryukhin.tm.exception.user.UserNotFoundException;
import ru.t1.nkiryukhin.tm.model.User;

import java.util.List;

public interface IUserService {

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User add(User user) throws UserNotFoundException;

    List<User> findAll();

    User findById(String id) throws IdEmptyException;

    User findByLogin(String login) throws LoginEmptyException;

    User findByEmail(String email) throws EmailEmptyException;

    User remove(User user) throws UserNotFoundException;

    User removeById(String id) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    User removeByEmail(String email) throws AbstractException;

    User setPassword(String id, String password) throws AbstractException;

    User updateUser(String id, String firstName, String lastName, String middleName) throws AbstractException;

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
